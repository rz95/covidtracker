#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Patient{
    char name[80];
    struct Patient* left;
    struct Patient* right;
};

struct Patient* searchNode(struct Patient* node, char name[80]){
    if(node!=NULL){
        if(strcmp(node->name,name)==0){
            return node;
        }
        else{
            if(node->left!=NULL){
                if(searchNode(node->left,name)!=NULL){
                    return searchNode(node->left, name);
                }
                else{
                    if(searchNode(node->right,name)!=NULL){
                        return searchNode(node->right, name);
                    }
                    else{
                        return NULL;
                    }
                }
            }
        }
    }
    return NULL;
}

struct Patient* buildTree(FILE* filename){
    struct Patient* head = (struct Patient*) malloc(sizeof(struct Patient));
    strcpy(head->name,"BlueDevil");
    head -> left = NULL;
    head -> right = NULL;

    while(1){
        char name1[80];
        char name2[80];
        fscanf(filename, "%s", name1);
        if(strcmp(name1,"DONE")==0){
            break;
        }
        fscanf(filename, "%s", name2);
        
        struct Patient* childrenNode = (struct Patient*) malloc(sizeof(struct Patient));
        strcpy(childrenNode -> name,name1);
        childrenNode -> left = NULL;
        childrenNode -> right = NULL;
        
        struct Patient* parentNode = (struct Patient*) malloc(sizeof(struct Patient));
        if(searchNode(head, name2)!=NULL){
            parentNode = searchNode(head, name2);
        }
        else{
            strcpy(parentNode->name, name2);
            parentNode -> left = NULL;
            parentNode -> right = NULL;
        }
        
        if(parentNode!= NULL){
            if(parentNode -> left != NULL){
                if(strcmp(parentNode->left->name, childrenNode->name)<0){
                    parentNode -> right = childrenNode;
                }
                else{
                    struct Patient* tempNode = (struct Patient*) malloc(sizeof(struct Patient));
                    tempNode = parentNode->left;
                    parentNode->left = childrenNode;
                    parentNode->right = tempNode;
                }
            }
            else{
                parentNode->left = childrenNode;
            }
        }
    }
    return head;
}

void printTree(struct Patient* node){
    if(node==NULL){
        return ;
    }
    printf("%s\n",node->name);
    printTree(node->left);
    printTree(node->right);
}

void freeUp(struct Patient* node){
    if(node==NULL){
        return ;
    }
    freeUp(node->left);
    freeUp(node->right);
    free(node);
}

int main(int argc, char** argv){
    FILE* f;
    f = fopen(argv[1],"r");
    struct Patient* head = (struct Patient*) malloc(sizeof(struct Patient));
    head = buildTree(f);
    printTree(head);
    freeUp(head);
    fclose(f);
    return 0;
}