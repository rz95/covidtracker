# Covidtracker

This is the homework assignment of *CS250: Computer Architecture*. 

It's an implementation of a binary search tree stored on the heap and uses DFS for printing,

coded respectively in C and Assembly.
