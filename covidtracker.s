.data
.align 2
name: .space 16
patient_prompt: .asciiz "Please enter patient:\n"
infector_prompt: .asciiz "Please enter infector:\n"
bluedevil: .asciiz "BlueDevil\n"
done: .asciiz "DONE\n"

.text
main:
sub $sp, $sp, 16
sw $ra, 0($sp) 
sw $s0, 4($sp) #head node
sw $s1, 8($sp) #changing patient node
sw $s2, 12($sp) #changing infector node

li $a0, 88 #dynamic allocation for head node
li $v0, 9 #$v0 = the address of the first byte of the dynamically allocated block
syscall

move $s0, $v0 #$s0 = the address of the first byte of head node

la $a0, bluedevil #$a0 = address of name of head node

move $a1, $a0 #strcpy source address = address of name of head node
move $a0, $s0 #strcpy destination address = address of the first byte of head node
jal strcpy

sw $0, 16($s0) #initialize head node's left & right node
sw $0, 20($s0)

loop:
    li $a0, 24 #dynamic allocation for patient node
    li $v0, 9 #$v0 = the address of the first byte of the dynamically allocated block
    syscall

    move $s1, $v0 #$s1 = the address of the first byte of patient node

    li $v0, 4
    la $a0, patient_prompt
    syscall

    li $v0, 8 #$a0 = address of name of patient node
    la $a0, name
    la $a1, 16
    syscall

    move $t2, $a0 #temporarily copy $a0 to $t2, $t2 = address of name of patient node

    la $a0, done 

    move $a1, $t2 #copy $t2 back to $a1, so to compare name of patient node with "DONE"
    jal strcmp
    beqz $v0, exit

    move $a1, $t2 #strcpy source address = address of name of patient node
    move $a0, $s1 #strcpy destination address = address of the first byte of patient node
    jal strcpy

    sw $0, 16($s1) #initialize patient node's left & right node
    sw $0, 20($s1)

    li $v0, 4
    la $a0, infector_prompt
    syscall

    li $v0, 8 #$a0 = address of name of infector node
    la $a0, name
    la $a1, 16
    syscall

    move $t3, $a0 #temporarily copy $a0 to $t3

    move $a1, $t3 # address of name of infector node is the 2nd argument
    move $a0, $s0 # address of the first byte of head node is the 1st argument
    jal search

    move $s2, $v1 # $v1 is the address of the found node
    jal connect #connect infector node & patient node
    j loop

exit: #"DONE"
    move $a0, $s0
    jal print
    lw $s2, 12($sp) #changing infector node  
    lw $s1, 8($sp) #changing patient node
    lw $s0, 4($sp) #head node
    lw $ra, 0($sp)  
    addi $sp, $sp, 16
    jr $ra

print: #$s0 = the address of the first byte of head node
    sub $sp, $sp, 8
    sw $ra, 0($sp)
    beqz $a0, return_print

    move $t7, $a0
    sw $t7, 4($sp)

    li $v0, 4 #print name of current node
    syscall

    lw $a0, 16($a0) #change left node to current node
    jal print

    lw $t7, 4($sp)
    move $a0, $t7
    lw $a0, 20($a0) #change right node to current node
    jal print

    lw $ra, 0($sp)
    addi $sp, $sp, 8
    jr $ra

return_print:
    lw $ra, 0($sp)
    addi $sp, $sp, 8
    jr $ra
    
connect: #$s1 is address of patient node, $s2 is address of infector node, connect them
    sub $sp, $sp, 4
    sw $ra, 0($sp)

    beqz $s2, return_connect
    lw $t5, 16($s2) #infector node's left node
    beqz $t5, insert_left

    move $a0, $t5 #compare infector node's left node with patient node
    move $a1, $s1
    jal strcmp

    bltz $v0, insert_right
    lw $t6, 16($s2) #need to switch left and right patient nodes, temp node copies infector's left node
    sw $s1, 16($s2) 
    sw $t6, 20($s2)

    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

return_connect:
    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

insert_left:
    sw $s1, 16($s2)
    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

insert_right:
    sw $s1, 20($s2)
    lw $ra, 0($sp)
    addi $sp, $sp, 4
    jr $ra

search: #$a0 = the address of the first byte of head node, $a1 = address of name of infector node (the name to search)
    sub $sp, $sp, 16
    sw $ra, 0($sp)
    sw $s4, 4($sp) 
    sw $s5, 8($sp)

    move $s4, $a0 #the node we are looking at
    move $s5, $a1 #the address of the name to search
    beqz $s4, notFound #current node null
    jal strcmp
    beqz $v0, found

    move $t4, $s4 #temporarily copy to $t4
    sw $t4, 12($sp) #original node
    lw $s4, 16($s4) #make left node current node
    move $a0, $s4
    move $a1, $s5
    beqz $s4, notFound #left node null
    jal search 
    bnez $v1, out

    lw $t4, 12($sp) #original node
    move $s4, $t4
    lw $s4, 20($s4) #make right node current node
    move $a0, $s4
    beqz $s4, notFound
    jal search 
    bnez $v1, out
out:    
    lw $s5, 8($sp)
    lw $s4, 4($sp) 
    lw $ra, 0($sp)
    addi $sp, $sp, 16
    jr $ra
found:
    move $v1, $s4
    lw $s5, 8($sp)
    lw $s4, 4($sp)
    lw $ra, 0($sp)
    addi $sp, $sp, 16
    jr $ra
notFound:
    li $v1, 0
    lw $s5, 8($sp)
    lw $s4, 4($sp) 
    lw $ra, 0($sp)
    addi $sp, $sp, 16
    jr $ra

# $a0 = dest, $a1 = src
strcpy:
	lb $t0, 0($a1)
	beq $t0, $zero, done_copying
	sb $t0, 0($a0)
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	j strcpy

	done_copying:
	jr $ra

# $a0, $a1 = strings to compare
# $v0 = result of strcmp($a0, $a1)
strcmp:
	lb $t0, 0($a0)
	lb $t1, 0($a1)

	bne $t0, $t1, done_with_strcmp_loop
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	bnez $t0, strcmp
	li $v0, 0
	jr $ra
		
	done_with_strcmp_loop:
	sub $v0, $t0, $t1
	jr $ra